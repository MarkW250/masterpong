# MasterPong

Super simple pong game, with a 'twist' :)

## Instructions
All assets and images are super small, so they are all part of the repo for now. Might be moved if more fancy things are added.

Therefore, it should be as simple as cloning the repo and opening the project in Unity.

Unity version currently being used: 2019.2.10f1

## Current notes
- "Options" menu does not do anything

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoalScript : MonoBehaviour {

    public PlayerInfo.PLAYER_NO Player;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.collider.CompareTag("Ball"))
        {
            GameMaster.OnGoal(Player, collision.collider.gameObject);
        }
    }
}

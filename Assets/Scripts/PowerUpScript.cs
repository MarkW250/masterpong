﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpScript : MonoBehaviour {

    public enum PowerUpTypeEnum
    {
        Dummy,
        BiggerPaddle,
        SmallerPaddle,
        ScreenRotateToggle
    }

    
    public static void SpawnNewPowerUp(Vector3 spawnLocation,  PowerUpTypeEnum type = PowerUpTypeEnum.Dummy)
    {
        GameObject powerUpPrefab = Resources.Load<GameObject>("PowerUp");
        GameObject newPower = Instantiate(powerUpPrefab, spawnLocation, new Quaternion());

        newPower.GetComponent<PowerUpScript>().SetPowerUpType(type);
    }


    private PowerUpTypeEnum _powerUpType = PowerUpTypeEnum.Dummy;

    public void SetPowerUpType(PowerUpTypeEnum newType)
    {
        _powerUpType = newType;
        SpriteRenderer myRenderer = GetComponent<SpriteRenderer>();
        myRenderer.sprite = Resources.Load<Sprite>("Sprite_PowerUp_" + newType.ToString());
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        PlayerInfo.PLAYER_NO playerToApply_No = PlayerInfo.PLAYER_NO.None;
        PlayerInfo playerToApply = null;

        if (collision.gameObject.CompareTag("Ball"))
        {
            BallController ballObject = collision.gameObject.GetComponent<BallController>();

            if (ballObject != null)
            {
                playerToApply_No = ballObject.LastHitPlayer;
            }
        }

        if (playerToApply_No != PlayerInfo.PLAYER_NO.None)
        {
            playerToApply = PlayerInfo.GetPlayerInfo(playerToApply_No);
        }

        // apply appropriate effect on player or game
        switch (_powerUpType)
        {
            case PowerUpTypeEnum.BiggerPaddle:
                {
                    if (playerToApply != null)
                    {
                        Transform playerTransform = playerToApply.transform;
                        float newYScale = playerTransform.localScale.y * 1.10f;

                        if (newYScale > 2.0f)
                        {
                            newYScale = 2.0f;
                        }

                        Vector3 newScale = new Vector3(playerTransform.localScale.x, newYScale);
                        playerTransform.localScale = newScale;
                    }
                    break;
                }

            case PowerUpTypeEnum.SmallerPaddle:
                {
                    if (playerToApply != null)
                    {
                        Transform playerTransform = playerToApply.transform;
                        float newYScale = playerTransform.localScale.y * 0.9f;

                        if (newYScale < 0.5f)
                        {
                            newYScale = 0.5f;
                        }

                        Vector3 newScale = new Vector3(playerTransform.localScale.x, newYScale);
                        playerTransform.localScale = newScale;
                    }
                    break;
                }

            case PowerUpTypeEnum.ScreenRotateToggle:
                {
                    GameMaster.MyGameMaster.ToggleScreenRotation();
                    break;
                }

            default:
                {
                    // do nothing
                    break;
                }

        }

        // TODO play some animation on destroy
        SpriteRenderer renderer = GetComponent<SpriteRenderer>();
        Destroy(this.gameObject);
    }
}

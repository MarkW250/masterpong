﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerScript : MonoBehaviour
{

    public float InitialPlayerSpeed = 1.0f;

    private string _playerInputName;
    private float _playerSpeed;
    private float _playerUpperBound;
    private float _playerLowerBound;
    private AiController _myAi = null;

    // Use this for initialization
    void Start ()
    {
        _playerInputName = "Vertical_" + GetComponent<PlayerInfo>().PlayerNumber.ToString();
        _playerSpeed = InitialPlayerSpeed;

        _playerUpperBound = GameMaster.MyGameMaster.PlayerStartUpperBound;
        _playerLowerBound = GameMaster.MyGameMaster.PlayerStartLowerBound;

        // At the moment, only player 2 can be AI
        if ((GetComponent<PlayerInfo>().PlayerNumber == PlayerInfo.PLAYER_NO.Player2) && (CrossSceneManager.NewMatchAiEnabled))
        {
            _myAi = new AiController(this, CrossSceneManager.NewMatchAiDifficulty);
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float input = Input.GetAxis(_playerInputName);

        if(_myAi != null)
        {
            // Get input value from AI
            input = _myAi.GetInput();
        }
        else
        {
            // Get input value from controller
            input = Input.GetAxis(_playerInputName);
        }

        float changeInPosition = Time.deltaTime * _playerSpeed * input;

        Vector3 newLocation = transform.localPosition;
        newLocation.Set(newLocation.x, newLocation.y + changeInPosition, newLocation.z);
        Vector3 paddleScale = transform.GetChild(0).lossyScale;

        // bound position according to player bounds
        if((newLocation.y + (paddleScale.y / 2.0f)) > _playerUpperBound)
        {
            newLocation.y = _playerUpperBound - (paddleScale.y / 2.0f);
        }

        if ((newLocation.y - (paddleScale.y / 2.0f)) < _playerLowerBound)
        {
            newLocation.y = _playerLowerBound + (paddleScale.y / 2.0f);
        }

        transform.localPosition = newLocation;
    }


    public float UpperBound
    {
        get { return _playerUpperBound; }
    }

    public float LowerBound
    {
        get { return _playerLowerBound; }
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


static class CrossSceneManager
{
    public static bool NewMatchAiEnabled = false;
    public static AiController.Difficulty NewMatchAiDifficulty = AiController.Difficulty.Easy;
}


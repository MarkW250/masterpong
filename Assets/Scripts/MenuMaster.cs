﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuMaster : MonoBehaviour
{
    public static MenuMaster MyMenuMaster;

    [Tooltip("Distance Between Menu Items")]
    public float MenuSpacing = 15.0f;

    [Tooltip("Input Debounce Time (s)")]
    public float MenuDebounceSec = 0.2f;

    [Tooltip("Font size of selected item")]
    public int MenuSelectedFontSize = 28;

    [Tooltip("Font size of non selected items")]
    public int MenuNormalFontSize = 25;

    public Text TextPrefab;
    public Text TitlePrefab;
    public Canvas MenuCanvas = null;
    public Text VersionText = null;

    private int _selectedIndex = 0;
    private float _debounceTimer = 0.0f;

    private MenuItem[] _menuItems;

    // Update is called once per frame
    void Update()
    {
        // Check if debounce timer is finished
        if (_debounceTimer <= 0.0f)
        {
            // Get Input
            float input = Input.GetAxisRaw("Menu_Select_Vertical");
            if(input != 0.0f)
            {
                // If something is being pressed, then do stuff
                ShiftMenuSelection(input);

                // Reset debounce timer
                _debounceTimer = MenuDebounceSec;
            }
        }
        else
        {
            _debounceTimer -= Time.deltaTime;
        }
        
        // If enter key is pressed, activate the currently selected menu item
        if(Input.GetButtonDown("Menu_Activate"))
        {
            _menuItems[_selectedIndex].OnActivate();
        }
    }

    // Use this for initialization
    void Awake()
    {
        if (MyMenuMaster == null)
        {
            MyMenuMaster = this;
        }
        else if (MyMenuMaster != this)
        {
            Destroy(gameObject);
        }

        // Set version text
        if(VersionText != null)
        {
            VersionText.text = "V" + Application.version;
        }

        // Set up main menu
        SetMenuItems_MainMenu();

        // make sure update funcitons are going 
        Time.timeScale = 1.0f;
    }

    public void ExecuteGameExit()
    {
        // delete the menu items
        foreach (MenuItem item in _menuItems)
        {
            Destroy(item.TextObject);
        }

        // create text to tell user that we are quitting
        Text tempTextBox = Instantiate(TextPrefab, new Vector3(0, 0, 0), new Quaternion()) as Text;
        tempTextBox.transform.SetParent(this.MenuCanvas.transform, false);
        tempTextBox.fontSize = MenuNormalFontSize;
        tempTextBox.text = "Exiting...";

        // Close the game
        Application.Quit();
    }


    /*
     * ShiftMenuSelection()
     * Shift menu selection up or down
     * 
     * in: float input: input value from input (-1 is down arrow, +1 is up arrow)
     */
    private void ShiftMenuSelection(float input)
    {
        // De-highlight current selected text
        _menuItems[_selectedIndex].TextObject.fontStyle = FontStyle.Normal;
        _menuItems[_selectedIndex].TextObject.fontSize = MenuNormalFontSize;


        // If input is negative, then increase menu index
        if (input < 0.0f)
        {
            _selectedIndex += 1;
        }
        else if(input > 0.0f)
        {
            // Otherise decrease selected index
            _selectedIndex -= 1;
        }

        // Make sure no over- or under- flow
        if (_selectedIndex < 0) { _selectedIndex = _menuItems.Length - 1; };
        if (_selectedIndex >= _menuItems.Length) { _selectedIndex = 0; };

        // Highlight new text
        _menuItems[_selectedIndex].TextObject.fontStyle = FontStyle.Bold;
        _menuItems[_selectedIndex].TextObject.fontSize = MenuSelectedFontSize;
    }

    private void SetMenuItems_MainMenu()
    {
        // LIST OF MAIN MENU ITEMS
        _menuItems = new MenuItem[]
        {
            /*              Menu Text           Menu Action Delegate                    */
            new MenuItem(   "Start 2 Player",   this.MenuAction_StartGame_2Player),
            new MenuItem(   "Start Vs. AI",     this.GoToAiMenu),
            new MenuItem(   "Exit",             this.MenuAction_Quit),
        };

        SetupMenuItems();
    }

    private void SetMenuItems_AiSelectionMenu()
    {
        List<MenuItem> menuList = new List<MenuItem>();

        // generate a list of menu items based on available difficulties
        foreach(string difficulty in System.Enum.GetNames(typeof(AiController.Difficulty)))
        {
            menuList.Add(new MenuItem(difficulty, this.MenuAction_StartGame_VsAI));
        }

        menuList.Add(new MenuItem("Back to Menu", this.GoToMainMenu));

        _menuItems = menuList.ToArray();

        SetupMenuItems();
    }

    /*
    * SetupMenuItems()
    * Nicely spaces out menu items
    */
    private void SetupMenuItems()
    {
        // first remove all existing texts
        Text[] currentTexts = Object.FindObjectsOfType<Text>();

        foreach(Text txt in currentTexts)
        {
            if (txt != VersionText)
            {
                Destroy(txt);
            }
        }


        float totalVerticalHeight = 0.0f;

        string GAME_TITLE = "MASTER PONG";

        // Calculate starting position based on number of items and size of items
        foreach (MenuItem item in _menuItems)
        {
            Text tempTextBox = Instantiate(TextPrefab, new Vector3(0, 0), new Quaternion()) as Text;
            tempTextBox.transform.SetParent(this.MenuCanvas.transform, false);
            tempTextBox.fontSize = MenuNormalFontSize;

            item.TextObject = tempTextBox;

            totalVerticalHeight += item.TextObject.rectTransform.sizeDelta.y;
        }
        // Add in spacing between menu items
        totalVerticalHeight += (MenuSpacing * (_menuItems.Length - 1));

        // Instantiate menu title
        Text titleBox = Instantiate(TitlePrefab, new Vector3(0, 0), new Quaternion()) as Text;
        titleBox.transform.SetParent(this.MenuCanvas.transform, false);
        titleBox.text = GAME_TITLE;

        // set top position
        float position = totalVerticalHeight / 2.0f;

        // put title above menu items
        titleBox.rectTransform.localPosition = new Vector3(0.0f, position + 2.0f * (MenuSpacing) + titleBox.rectTransform.sizeDelta.y);

        // loop through all menut items and place them
        foreach (MenuItem item in _menuItems)
        {
            item.TextObject.rectTransform.localPosition = new Vector3(0.0f, position);
            position -= item.TextObject.rectTransform.sizeDelta.y;
            position -= MenuSpacing;

            item.TextObject.fontSize = MenuNormalFontSize;
        }

        // reset selected index
        _selectedIndex = 0;

        // Shift menu selection with value zero will highlight initial selection
        ShiftMenuSelection(0.0f);
    }

    /* DELEGATE FUNCTIONS FOR MENU ACITONS */

    // Delegate function for starting a new 2 player game 
    private void MenuAction_StartGame_2Player()
    {
        CrossSceneManager.NewMatchAiEnabled = false;
        SceneManager.LoadScene("Level0", LoadSceneMode.Single);
    }

    // Delegate function for starting a game against AI
    private void MenuAction_StartGame_VsAI()
    {
        CrossSceneManager.NewMatchAiEnabled = true;
        CrossSceneManager.NewMatchAiDifficulty = (AiController.Difficulty)(_selectedIndex);
        SceneManager.LoadScene("Level0", LoadSceneMode.Single);
    }

    // Delegate function for quitting
    private void MenuAction_Quit()
    {
        ExecuteGameExit();
    }

    // Delegate to got back to main menu
    private void GoToMainMenu()
    {
        SetMenuItems_MainMenu();
    }

    // Delegate to go to AI selection menu
    private void GoToAiMenu()
    {
        SetMenuItems_AiSelectionMenu();
    }
}

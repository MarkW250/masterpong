﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour {

    [Tooltip("Starting velocity magnitude")]
    public float StartingSpeed = 5.0f;

    [Tooltip("Starting angle of velocity in degrees")]
    public float StartingAngle = 0.0f;

    [Tooltip("Speed multiplier applied to ball after hitting a paddle")]
    public float SpeedMultiplier = 1.1f;

    [Tooltip("Maximum speed that ball can travel")]
    public float MaxSpeed = 20.0f;

    [Tooltip("Maximum angle that ball can bounce off of paddle")]
    public float MaxBounceAngle = 60.0f;

    private GameObject _lastHitObject = null;
    private PlayerInfo.PLAYER_NO _lastHitPlayer = PlayerInfo.PLAYER_NO.None;
    private Vector2 _currentVelocity = new Vector2();


    public PlayerInfo.PLAYER_NO LastHitPlayer
    {
        get
        {
            return _lastHitPlayer;
        }
    }

    // Use this for initialization
    void Start ()
    {
        float startingAngleRadians = StartingAngle * Mathf.PI / 180.0f;
        float startVelocityX = StartingSpeed * Mathf.Cos(startingAngleRadians);
        float startVelocityY = StartingSpeed * Mathf.Sin(startingAngleRadians);


        // start direction towards losing player to help them out
        ControllerScript[] players = Object.FindObjectsOfType<ControllerScript>();
        foreach(ControllerScript player in players)
        {
            if(PlayerInfo.GetPlayerNumber(player.gameObject) == GameMaster.LastPlayerToLoseLife)
            {
                if((player.transform.position.x - this.transform.position.x) < 0)
                {
                    startVelocityX *= -1.0f;
                } 
            }
        }


        Rigidbody2D myRigidBody = GetComponent<Rigidbody2D>();
        Vector2 newVelocity = new Vector2(startVelocityX, startVelocityY);
        myRigidBody.velocity = newVelocity;

        _currentVelocity = newVelocity;

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Rigidbody2D myRigidBody = GetComponent<Rigidbody2D>();

        if (collision.gameObject.CompareTag("Player"))
        {
            // First save the player number of the player we have come into contact with
            _lastHitPlayer = PlayerInfo.GetPlayerNumber(collision.gameObject);


            // collided with player paddle, let's bounce off at an angle that is proportional to the position of the ball on the paddle
            float newDirection = _currentVelocity.x / Mathf.Abs(_currentVelocity.x) * -1.0f;

            float angleMultiplier = (myRigidBody.position.y - collision.rigidbody.position.y) / (collision.collider.bounds.size.y / 2.0f);

            // bound angle multiplier
            if (angleMultiplier > 1.0f) { angleMultiplier = 1.0f; }
            if (angleMultiplier < -1.0f) { angleMultiplier = -1.0f; }

            float newVelocityAngle = angleMultiplier * MaxBounceAngle;

            float newAngleRadians = newVelocityAngle * Mathf.PI / 180.0f;
            float newVelocityMagnitude = _currentVelocity.magnitude * SpeedMultiplier;
            if(newVelocityMagnitude > MaxSpeed)
            {
                newVelocityMagnitude = MaxSpeed;
            }
            float newVelocityX = newVelocityMagnitude * Mathf.Cos(newAngleRadians) * newDirection;
            float newVelocityY = newVelocityMagnitude * Mathf.Sin(newAngleRadians);

            Vector2 newVelocity = new Vector2(newVelocityX, newVelocityY);
            myRigidBody.velocity = newVelocity;
            _currentVelocity = newVelocity;

            // Notify game master of this shannanigans
            GameMaster.OnBallPaddleHit();
        }
        else if(collision.gameObject.CompareTag("Perimeter"))
        {
            // It was found that if the ball is going too fast, then this collision event triggers when the ball is already beyond the boundary and the ball travels back toward the boundary,
            // it then hits it again and goes back into the abiss.
            // therefore we make sure we bounce off only we have not just hit this wall
            if (_lastHitObject != collision.gameObject)
            {
                // assume an upper or lower wall and just change y direction
                Vector2 newVelocity = new Vector2(_currentVelocity.x, _currentVelocity.y * -1.0f);
                myRigidBody.velocity = newVelocity;
                _currentVelocity = newVelocity;
            }
            else
            {
                Debug.LogWarning("Hitting same perimeter twice");
            }
        }

        _lastHitObject = collision.gameObject;
    }

    private void Update()
    {
        Rigidbody2D myRigidBody = GetComponent<Rigidbody2D>();
        // sanity check on velocity
        // Unfortuantely to get accurate collision detection we've had to make the ball dynamic physics
        // Due to this the velocity gets lost everytime we hit a power-up. I did not feel like finding
        // a proper fix
        if (myRigidBody.velocity != _currentVelocity)
        {
            if ((_lastHitObject != null) && (_lastHitObject.gameObject.CompareTag("Perimeter")))
            {
                Debug.LogWarning("Velocity lost - resetting");
                Vector2 newVelocity = new Vector2(_currentVelocity.x, _currentVelocity.y * -1.0f);
                _currentVelocity = newVelocity;
            }
            myRigidBody.velocity = _currentVelocity;
        }
    }

}

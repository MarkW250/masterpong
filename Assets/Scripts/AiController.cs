﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


class AiController
{
    public enum Difficulty
    {
        Easy        = 0,
        Medium      = 1,
        Hard        = 2,
        Impossible  = 3
    }

    // private attributes
    private Difficulty _myDifficulty;
    private ControllerScript _myPaddle;

    private float _positionVarianceRange = 1.0f;                       // amount of distance that the AI can go to away from the ideal location
    private float _rotatingScreenPositionVarianceRange = 1.5f;         // amount of distance that the AI can go to away from the ideal location while screen is rotating
    private float _correctionProb = 0.1f;                              // probability of correcting mistake when going in wrong direction
    private float _adjustmentProb = 0.03f;                             // probability of making adjustment to or location
    private float _baseMistakeProb = 0.1f;                             // probability of going wrong way under normal conditions
    private float _rotatingScreenMistakeProbability = 0.5f;            // probability of going wrong way under rotating screen
    

    // state variables
    private enum AI_State
    {
        State_0,
        State_1
    }
    private AI_State _myState = AI_State.State_0;
    private float _aimedLocation = 0.0f;
    private float _idealLocation = -1.0f;       // this should not be 0.0f to force paddle to make a new desination calculation

    private float _timeCounter = 0.0f;
    private float _lastReturn = 0.0f;
    private float _nextDelay = 0.0f;

    // constructor
    public AiController(ControllerScript paddleObject, Difficulty difficulty)
    {
        _myDifficulty = difficulty;
        _myPaddle = paddleObject;

        switch(_myDifficulty)
        {
            case Difficulty.Easy:
                _positionVarianceRange = 2.0f;
                _rotatingScreenPositionVarianceRange = 3.0f;
                _correctionProb = 0.1f;
                _adjustmentProb = 0.03f;
                _baseMistakeProb = 0.3f;
                _rotatingScreenMistakeProbability = 0.6f;
                break;

            case Difficulty.Medium:
                _positionVarianceRange = 1.0f;
                _rotatingScreenPositionVarianceRange = 1.5f;
                _correctionProb = 0.1f;
                _adjustmentProb = 0.03f;
                _baseMistakeProb = 0.1f;
                _rotatingScreenMistakeProbability = 0.5f;
                break;

            case Difficulty.Hard:
                _positionVarianceRange = 0.7f;
                _rotatingScreenPositionVarianceRange = 1.2f;
                _correctionProb = 0.1f;
                _adjustmentProb = 0.01f;
                _baseMistakeProb = 0.05f;
                _rotatingScreenMistakeProbability = 0.1f;
                break;

            case Difficulty.Impossible:
                _positionVarianceRange = 0.0f;
                _rotatingScreenPositionVarianceRange = 0.0f;
                _correctionProb = 0.0f;
                _adjustmentProb = 0.0f;
                _baseMistakeProb = 0.0f;
                _rotatingScreenMistakeProbability = 0.0f;
                break;

            default:
                // just use values that were initialised in class
                break;
        }
    }

    // Get input from AI player
    public float GetInput()
    {
        float retVal = _lastReturn;

        _timeCounter += Time.deltaTime;

        // Every so often, calculate what we should be doing right now
        if (_timeCounter >= _nextDelay)
        {
            _timeCounter = 0.0f;

            float newIdealLocation = getIdealLocation();

            // check if new ideal location is a certain percentage away
            if (   ((newIdealLocation) < (_idealLocation - _myPaddle.transform.lossyScale.y * 0.05)) 
                || ((newIdealLocation) > (_idealLocation + _myPaddle.transform.lossyScale.y * 0.05)))
            {
                // we are changing our destination
                _idealLocation = newIdealLocation;

                // aim paddle so that the ball will hit the side of it about half of the time to change up AI behaviour a little to avoid 'dead locks'
                if (Random.value < 0.5f)
                {
                    float paddleCurrLocation = _myPaddle.transform.position.y;
                    _aimedLocation = (paddleCurrLocation < _idealLocation) ?
                                        (_idealLocation - (_myPaddle.transform.lossyScale.y / 2.0f)) :
                                        (_idealLocation + (_myPaddle.transform.lossyScale.y / 2.0f));
                }

                // add some error to our aimed destination
                float positionVar = GameMaster.IsScreenRotating() ? _rotatingScreenPositionVarianceRange : _positionVarianceRange;
                _aimedLocation = Random.Range(_idealLocation - positionVar, _idealLocation + positionVar);

                // determine if mistake should be made
                float mistakeProb = (GameMaster.IsScreenRotating()) ? _rotatingScreenMistakeProbability : _baseMistakeProb;
                bool makeMistake = (Random.value < mistakeProb) ? true : false;

                if (makeMistake)
                {
                    // next decision to be made in 300 ms
                    _nextDelay = 0.3f;
                    // Go to state 0 where we will calculate probability of correcting our course
                    _myState = AI_State.State_0;
                }
                else
                {
                    // Next decision to be made in 100 ms
                    _nextDelay = 0.1f;
                    // Go to state 1 where we will go towards our destination
                    _myState = AI_State.State_1;

                }

                retVal = getDirOfLocation(_aimedLocation);

                if (makeMistake)
                {
                    retVal = retVal * -1.0f;
                }
            }
            else
            {
                // handle state machine
                switch (_myState)
                {
                    case AI_State.State_0:
                        // determine if course should be corrected
                        if (Random.value < _correctionProb)
                        {
                            // go to state 1
                            _myState = AI_State.State_1;

                            // wait 10ms
                            _nextDelay = 0.01f;
                        }
                        else
                        {
                            // if not, wait another 100ms
                            _nextDelay = 0.1f;
                        }
                        break;

                    case AI_State.State_1:
                        // destination reached?
                        // if so, output = 0
                        retVal = getDirOfLocation(_aimedLocation);
                        // go to state 2

                        if (retVal == 0.0f)
                        {
                            // determine if we should try marka re-calculation of our position
                            if (Random.value < _adjustmentProb)
                            {
                                // setting ideal location will force some movement
                                _idealLocation = int.MaxValue;
                            }
                            // wait 100ms
                            _nextDelay = 0.1f;
                        }
                        else
                        {
                            _nextDelay = 0.001f;
                        }

                        break;


                    default:
                        break;
                }
            }
        }

        _lastReturn = retVal;

        return retVal;
    }


    private float getIdealLocation()
    {
        float predictedPointOfBall = 0.0f;

        // fetch ball object(s) in scene currently
        BallController[] balls = Object.FindObjectsOfType<BallController>();

        if(balls.Length > 0)
        {
            // check which ball is gonna get to us first
            BallController ballToTrack = null;
            float lowestTime = 0.0f;

            foreach (BallController ball in balls)
            {
                float distance = _myPaddle.gameObject.transform.position.x - ball.gameObject.transform.position.x;
                float velocity = ball.gameObject.GetComponent<Rigidbody2D>().velocity.x;

                float time = distance / velocity;

                // only track balls coming toward us
                if (time >= 0.0f)
                {
                    if ((lowestTime == 0.0f) || (time < lowestTime))
                    {
                        // this is the ball that will get to us first for now
                        ballToTrack = ball;
                        lowestTime = time;
                    }
                }
            }

            if (ballToTrack != null)
            {
                // predict where the ball will get to when it reaches us
                bool foundPoint = false;
                int i = 0;

                float myX = _myPaddle.gameObject.transform.position.x;
                float myY = _myPaddle.gameObject.transform.position.y;

                float upperBound = _myPaddle.UpperBound - (ballToTrack.transform.lossyScale.y / 2.0f);
                float lowerBound = _myPaddle.LowerBound + (ballToTrack.transform.lossyScale.y / 2.0f);

                float trackingX = ballToTrack.gameObject.transform.position.x;
                float trackingY = ballToTrack.gameObject.transform.position.y;

                float velocityX = ballToTrack.gameObject.GetComponent<Rigidbody2D>().velocity.x;
                float velocityY = ballToTrack.gameObject.GetComponent<Rigidbody2D>().velocity.y;
                float veloctyMag = ballToTrack.gameObject.GetComponent<Rigidbody2D>().velocity.magnitude;

                while ((foundPoint == false) && (i++ < 1000))
                {
                    /*             x
                     *   ---------------------
                     *    |                 |
                     *  y |                 |
                     *    o ----------------|
                     *                      |
                     */
                    // calculate angle between ball and corner point 
                    float angleBallToCorner = 0.0f;
                    float boundary = 0.0f;
                    if (velocityY >= 0.0f)
                    {
                        boundary = upperBound;
                    }
                    else
                    {
                        boundary = lowerBound;
                    }

      
                    if (trackingY == boundary)
                    {
                        angleBallToCorner = 90.0f;
                    }
                    else
                    {
                        angleBallToCorner = Mathf.Abs(Mathf.Atan((myX - trackingX) / (boundary - trackingY)));
                    }

                    // now get angle of velocity
                    float ballVelocityAngle = Mathf.Abs(Mathf.Asin(velocityX / veloctyMag));

                    if(ballVelocityAngle >= angleBallToCorner)
                    {
                        // the ball will have no more rebounds - hooray!
                        float timeToMe = (myX - trackingX) / (velocityX);
                        predictedPointOfBall = trackingY + velocityY * timeToMe;
                        foundPoint = true;
                    }
                    else
                    {
                        // ball is going to rebound, so set new positions of ball for next iteration
                        float timeToWall = Mathf.Abs((boundary - trackingY) / (velocityY));
                        trackingX = trackingX + velocityX * timeToWall;
                        trackingY = boundary;
                        velocityY = velocityY * (-1.0f);
                    }
                }  
            }
        }

        return predictedPointOfBall;
    }


    private float getDirOfLocation(float destination)
    {
        // aim paddle so that the ball will hit the side of it
        float paddleCurrLocation = _myPaddle.transform.position.y;

        float retVal = 0.0f;

        // allow couple percent leeway to not look like twitching idiot
        if ((destination - _myPaddle.transform.lossyScale.y * 0.05) > paddleCurrLocation)
        {
            retVal = 1.0f;
        }
        else if ((destination + _myPaddle.transform.lossyScale.y * 0.05) < paddleCurrLocation)
        {
            retVal = -1.0f;
        }
        else
        {
            retVal = 0.0f;
        }

        return retVal;
    }
}

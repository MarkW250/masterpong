﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuItem
{
    public delegate void ActivateDelegate();

    public ActivateDelegate OnActivate = null;

    public string MenuText
    {
        get { return _menuText; }
        set
        {
            _menuText = value;
            if(_textObject != null)
            {
                _textObject.text = _menuText;
            }
        }
    }

    public Text TextObject
    {
        get { return _textObject; }
        set
        {
            _textObject = value;
            if(_textObject != null)
            {
                _textObject.text = _menuText;
            }
        }
    }


    private Text _textObject = null;
    private string _menuText = ""; 


    public MenuItem(string menuText, ActivateDelegate del)
    {
        OnActivate = del;
        _menuText = menuText;
    }
}

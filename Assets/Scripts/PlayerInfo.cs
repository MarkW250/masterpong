﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class PlayerInfo : MonoBehaviour
{
    private int _myScore = 0;

    public static List<PlayerInfo> PlayerInfos = new List<PlayerInfo>();

    public enum PLAYER_NO
    {
        None,
        Player1,
        Player2
    }

    public PlayerInfo.PLAYER_NO PlayerNumber;
    public string PlayerName = "";


    public int PlayerScore
    {
        set
        {
            _myScore = value;
            GameMaster.MyGameMaster.UpdatePlayerScoreText(PlayerNumber, _myScore.ToString());
        }
        get
        {
            return _myScore;
        }
    }

    private void Awake()
    {
        PlayerInfos.Add(this);

        if(PlayerName == "")
        {
            PlayerName = PlayerNumber.ToString();
        }
    }

    private void OnDestroy()
    {
        PlayerInfos.Remove(this);
    }

    public static PlayerInfo GetPlayerInfo(PLAYER_NO playerNo)
    {
        PlayerInfo ret = null;

        foreach(PlayerInfo p in PlayerInfos)
        {
            if(p.PlayerNumber == playerNo)
            {
                ret = p;
            }
        }

        return ret;
    }

    public static PlayerInfo.PLAYER_NO GetPlayerNumber(GameObject gameObject)
    {
        PlayerInfo thisInfo = gameObject.GetComponent<PlayerInfo>();
        if(thisInfo != null)
        {
            return thisInfo.PlayerNumber;
        }
        return PLAYER_NO.None;
    }
}

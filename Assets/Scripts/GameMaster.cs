﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;



public class GameMaster : MonoBehaviour {

    public static GameMaster MyGameMaster;
    public static PlayerInfo.PLAYER_NO LastPlayerToLoseLife = PlayerInfo.PLAYER_NO.Player1;

    public static void OnGoal(PlayerInfo.PLAYER_NO playerNo, GameObject BallObject)
    {
        PlayerInfo player = PlayerInfo.GetPlayerInfo(playerNo);
        player.PlayerScore--;
        LastPlayerToLoseLife = playerNo;

        // Destroy ball and spawn a new one
        Destroy(BallObject);

        // Spawn a new ball
        MyGameMaster.StartNewRound();
    }


    public static void OnBallPaddleHit()
    {
        MyGameMaster.SpawnRandomPowerUp();
    }

    public static bool IsScreenRotating()
    {
        return MyGameMaster.ScreenRotating;
    }


    [Tooltip("Starting upper bound of player paddles")]
    public float PlayerStartUpperBound = 4.8f;

    [Tooltip("Starting lower bound of player paddles")]
    public float PlayerStartLowerBound = -4.8f;

    [Tooltip("PowerUp Vertical Upper Bound")]
    public float PowerUpUpperBound = 4.8f;

    [Tooltip("PowerUp Vertical Lower Bound")]
    public float PowerUpLowerBound = -4.8f;

    [Tooltip("PowerUp Horizontal Upper Bound")]
    public float PowerUpRightBound = 4.8f;

    [Tooltip("PowerUp Horizontal Lower Bound")]
    public float PowerUpLeftBound = -4.8f;

    [Tooltip("How many lives you begin with")]
    public int StartingLives = 6;

    [Tooltip("Rotate rate (degrees per second) when screen is rotating")]
    public float RotateRate = 25;

    public float BallSpawningTime = 3.0f;

    public Canvas GameCanvas = null;

    public GameObject BallPrefab;
    public GameObject MainCamera;

    float _ballSpawningTimeLeft = 0.0f;
    bool _gameOver = false;
    bool _screenRotating = false;
    bool _inPauseMenu = false;

    public void StartNewRound()
    {
        // Here we first check the player scores and if all players are eliminated except one then the game is over
        int remainingPlayers = 2; // TODO think of a way to know how many players we have
        PlayerInfo potentialWinner = null;


        foreach (PlayerInfo.PLAYER_NO playerNo in System.Enum.GetValues(typeof(PlayerInfo.PLAYER_NO)))
        {
            PlayerInfo player = PlayerInfo.GetPlayerInfo(playerNo);
            if (player != null)
            {
                if (player.PlayerScore <= 0)
                {
                    // This player has lost!
                    remainingPlayers--;
                }
                else
                {
                    potentialWinner = player;
                }
            }
        }

        // If only one player is left, then we have a winner
        if((remainingPlayers == 1) && (potentialWinner != null))
        {
            UpdateText("Text_CountDownTimer", string.Format("{0} is the Winner! \n Play Again? (Y/N)", potentialWinner.PlayerName));
            _gameOver = true;
        }

        // If game is not over then spawn another ball
        if (_gameOver == false)
        {
            _ballSpawningTimeLeft = BallSpawningTime;
        }
    }

    public bool ScreenRotating
    {
        get { return _screenRotating; }
    }

    public void ToggleScreenRotation()
    {
        _screenRotating = !_screenRotating;
    }

    #region POWER UP HANDLING
    /************************************** POWER UP HANDLING *******************************************/
    // struct contining power type and its corresponding probability of spawning
    private struct PowerUpProbStruct
    {
        public PowerUpScript.PowerUpTypeEnum PowerUpType;
        public float PowerUpProbability;

        public PowerUpProbStruct(PowerUpScript.PowerUpTypeEnum type, float probability)
        {
            this.PowerUpType = type;
            this.PowerUpProbability = probability;
        }
    }

    /* BELOW TABLE LISTS POWER UP PROBABILITY SPAWNING */
    private PowerUpProbStruct[] _powerUpProbs =
    {
        /*                      Power Up Type,                                   Probability */
        new PowerUpProbStruct( PowerUpScript.PowerUpTypeEnum.BiggerPaddle,       25.0f ),
        new PowerUpProbStruct( PowerUpScript.PowerUpTypeEnum.SmallerPaddle,      25.0f ),
        new PowerUpProbStruct( PowerUpScript.PowerUpTypeEnum.ScreenRotateToggle, 25.0f )
    };

    /*
     * Method to spawn a random power up in the configured game boundaries
     */
    public void SpawnRandomPowerUp()
    {
        float spawningX = Random.Range(PowerUpLeftBound, PowerUpRightBound);
        float spawningY = Random.Range(PowerUpLowerBound, PowerUpUpperBound);

        PowerUpScript.PowerUpTypeEnum powerUpType = PowerUpScript.PowerUpTypeEnum.Dummy;
        float PowerRoll = Random.Range(0.0f, 100.0f);

        float counter = 0.0f;
        foreach(PowerUpProbStruct powerUpProb in _powerUpProbs)
        {
            if((PowerRoll >= counter) && (PowerRoll < (counter + powerUpProb.PowerUpProbability)))
            {
                powerUpType = powerUpProb.PowerUpType;
                break;
            }
            counter += powerUpProb.PowerUpProbability;
        }

        if(PowerRoll <= 25.0f)
        {
            powerUpType = PowerUpScript.PowerUpTypeEnum.BiggerPaddle;
        }
        else if (PowerRoll <= 50.0f)
        {
            powerUpType = PowerUpScript.PowerUpTypeEnum.SmallerPaddle;
        }
        else if (PowerRoll <= 75.0f)
        {
            powerUpType = PowerUpScript.PowerUpTypeEnum.ScreenRotateToggle;
        }

        if (powerUpType != PowerUpScript.PowerUpTypeEnum.Dummy)
        {
            PowerUpScript.SpawnNewPowerUp(new Vector3(spawningX, spawningY, 0), powerUpType);
        }
    }
    #endregion

    /*
     * Update the text of a player score
     */
    public void UpdatePlayerScoreText(PlayerInfo.PLAYER_NO playerNo, string newScore)
    {
        UpdateText("Text_" + playerNo.ToString() + "_Score", newScore);
    }

    /*
     * Set the version text
     */
    private void SetVersionText()
    {
        UpdateText("Text_Version", "V" + Application.version);
    }

    private void Awake()
    {
        // set static master to this instance
        MyGameMaster = this;
    }

    // Use this for initialization
    void Start()
    {
        SetVersionText();
        
        _gameOver = false;

        // reset scores
        resetLives();

        MyGameMaster.StartNewRound();
    }

    private void OnGUI()
    {
        // OnGUI is used for debugging and dev tools
        /*
        PlayerInfo player = PlayerInfo.GetPlayerInfo(PlayerInfo.PLAYER_NO.Player1);
        GUI.Label(new Rect(Screen.width - 100, 10,100,50), "Player 1 Score: " + player.PlayerScore);

        player = PlayerInfo.GetPlayerInfo(PlayerInfo.PLAYER_NO.Player2);
        GUI.Label(new Rect(10,10,100,50), "Player 2 Score: " + player.PlayerScore);
        */
    }

    private void Update()
    {
        if (_inPauseMenu == false)
        {
            if (_ballSpawningTimeLeft > 0.0f)
            {
                _ballSpawningTimeLeft -= Time.deltaTime;
                if (_ballSpawningTimeLeft <= 0.0f)
                {
                    _ballSpawningTimeLeft = 0.0f;
                    GameObject newBall = Instantiate(BallPrefab, new Vector3(0, 0, 0), new Quaternion());
                    UpdateText("Text_CountDownTimer", "");
                }
                else
                {
                    UpdateText("Text_CountDownTimer", string.Format("{0:F1}", _ballSpawningTimeLeft));
                }
            }
            else if (_gameOver == true)
            {
                if (Input.GetKey(KeyCode.Y) == true)
                {
                    // reload scene to start a new game
                    StartNewMatch();
                }
                else if (Input.GetKey(KeyCode.N) == true)
                {
                    // exit game
                    SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);
                }
            }

            if (_gameOver == false)
            {
                // Here game is in play. 

                // if screen is rotating then rotate it (duh)
                if (_screenRotating)
                {
                    Transform cameraTransform = MainCamera.transform;
                    cameraTransform.Rotate(0.0f, 0.0f, (RotateRate * Time.deltaTime));
                }
            }

            if (Input.GetKeyDown("escape"))
            {
                // go to pause menu
                UpdateText("Text_CountDownTimer", "Game Paused\nPress Q to quit or ESC to continue playing");
                _inPauseMenu = true;
                Time.timeScale = 0.0f;
            }
        }
        else
        {
            // we are in pause menu
            if (Input.GetKeyDown("escape"))
            {
                // continue playing
                UpdateText("Text_CountDownTimer", "");
                _inPauseMenu = false;
                Time.timeScale = 1.0f;
            }
            else if(Input.GetKeyDown(KeyCode.Q))
            {
                // exit
                SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);
            }
        }
    }

    private void resetLives()
    {
        // reset scores
        foreach (PlayerInfo.PLAYER_NO playerNo in System.Enum.GetValues(typeof(PlayerInfo.PLAYER_NO)))
        {
            PlayerInfo player = PlayerInfo.GetPlayerInfo(playerNo);
            if (player != null)
            {
                player.PlayerScore = StartingLives;
            }
        }
    }


    private void StartNewMatch()
    {
        // simply reload the scene
        SceneManager.LoadScene("Level0", LoadSceneMode.Single);
    }


    /*
     * Update the text of a canvas text object
     * */
    private void UpdateText(string textObjectTag, string newText)
    {
        Text[] TextObjects = GameCanvas.GetComponentsInChildren<Text>();
        foreach (Text textObject in TextObjects)
        {
            if(textObject.CompareTag(textObjectTag))
            {
                textObject.text = newText;
                break;
            }
        }
    }


}
